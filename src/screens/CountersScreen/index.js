import React from 'react';
import {StatusBar} from 'react-native';
import {useSelector} from 'react-redux';
import {Container, Content, List, Text} from './styles';
import Counter from '../../components/Counter';
import Header from '../../components/Header';

const CountersScreen = () => {
  const counters = useSelector((state) => state.counters);

  return (
    <Container>
      <StatusBar barStyle="light-content" backgroundColor="#001c47" />
      <Header title="Counters" />
      <Content>
        {counters.length === 0 ? (
          <Text>Nenhum contador criado</Text>
        ) : (
          <List
            data={counters}
            renderItem={({item, index}) => (
              <Counter data={item} index={index} />
            )}
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
          />
        )}
      </Content>
    </Container>
  );
};

export default CountersScreen;
