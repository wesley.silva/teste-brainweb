import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background: #2983c9;
`;

export const Content = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1;
  padding: 10px;
`;

export const ContainerText = styled.View``;

export const List = styled.FlatList`
  width: 100%;
`;

export const Text = styled.Text`
  color: #fafafa;
  font-size: 18px;
  font-weight: 600;
`;
