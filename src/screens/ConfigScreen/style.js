import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background: #2983c9;
`;

export const Content = styled.View`
  flex: 1;
  justify-content: space-between;
  padding: 20px;
`;

export const Title = styled.Text`
  font-size: 26px;
  color: #3a3a3a;
  margin-bottom: 15px;
  font-weight: bold;
`;

export const ContainerButtons = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const ContainerControls = styled.View`
  width: 100%;
  padding: 40px 10%;
  border: 3px dashed #ccc;
  border-radius: 5px;
  flex-direction: row;
  justify-content: space-evenly;
  align-self: center;
  align-items: center;
`;

export const ContentValue = styled.View``;
