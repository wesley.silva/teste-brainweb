import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import Toast from 'react-native-tiny-toast';

import {
  ContainerControls,
  ContainerButtons,
  ContentValue,
  Container,
  Content,
  Title,
} from './style';

import Header from '../../components/Header';
import ControlButton from '../../components/ControlButton';
import OptionButton from '../../components/OptionButton';

const ConfigScreen = () => {
  const dispatch = useDispatch();
  const {counters, indexSelected, sequence} = useSelector((state) => state);

  const newCounter = () => {
    dispatch({
      type: 'NEW_COUNTER',
    });
    Toast.show(`Counter ${sequence} adicionado`);
  };

  const removeCounter = () => {
    const idCounter = counters[indexSelected].id;

    dispatch({
      type: 'REMOVE_COUNTER',
    });
    Toast.show(`Counter ${idCounter} removido`);
  };

  const incrementValue = () => {
    dispatch({
      type: 'INCREMENT',
    });
    Toast.show(messageToast());
  };

  const decrementValue = () => {
    dispatch({
      type: 'DECREMENT',
    });
    Toast.show(messageToast());
  };

  const resetValue = () => {
    dispatch({
      type: 'RESET',
    });
    Toast.show(messageToast());
  };

  const messageToast = () => {
    const valueCounter = counters[indexSelected].value;
    const msg = `Counter ${
      counters[indexSelected].id
    } - Valor: ${valueCounter.toString().padStart(4, '0')}`;
    return msg;
  };

  return (
    <Container>
      <Header title="Config" />
      <Content>
        <ContentValue>
          <Title>Counters</Title>
          <ContainerButtons>
            <OptionButton
              name="notification"
              title={'Add counter'}
              onPress={newCounter}
            />
            <OptionButton
              name="trash"
              title={'Remove counter'}
              onPress={removeCounter}
            />
          </ContainerButtons>
        </ContentValue>

        <ContentValue>
          <Title>Selected Counter</Title>
          <ContainerControls>
            <ControlButton
              name="minus"
              title="Decrement"
              onPress={decrementValue}
            />
            <ControlButton name="ccw" title="Reset" onPress={resetValue} />
            <ControlButton
              name="plus"
              title="Increment"
              onPress={incrementValue}
            />
          </ContainerControls>
        </ContentValue>
      </Content>
    </Container>
  );
};

export default ConfigScreen;
