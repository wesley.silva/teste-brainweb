const initialState = {
  sequence: 1,
  indexSelected: 0,
  counters: [],
};

const CountReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SELECT_COUNTER':
      return {...state, indexSelected: action.payload.index};

    case 'NEW_COUNTER': {
      return {
        ...state,
        indexSelected: state.counters.length,
        sequence: state.sequence + 1,
        counters: [...state.counters, {value: 0, id: state.sequence++}],
      };
    }

    case 'INCREMENT': {
      const temp = [...state.counters];
      temp[state.indexSelected].value++;
      return {
        ...state,
        counters: temp,
      };
    }

    case 'DECREMENT': {
      const temp = [...state.counters];
      if (temp[state.indexSelected].value !== 0) {
        temp[state.indexSelected].value--;
      }
      return {
        ...state,
        counters: temp,
      };
    }

    case 'RESET': {
      const temp = [...state.counters];
      temp[state.indexSelected].value = 0;
      return {
        ...state,
        counters: temp,
      };
    }

    case 'REMOVE_COUNTER': {
      const temp = [...state.counters];
      temp.splice(state.indexSelected, 1);
      return {
        ...state,
        counters: temp,
        indexSelected: 0,
      };
    }
  }

  return state;
};

export default CountReducer;
