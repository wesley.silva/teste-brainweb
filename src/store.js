import {createStore} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CountReducer from '../src/reducer/CountReducer';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, CountReducer);
const store = createStore(persistedReducer);
const persistor = persistStore(store);

export {store, persistor};
