import styled from 'styled-components/native';

export const Container = styled.View`
  height: 70px;
  flex-direction: row;
  background: #000f26;
`;

export const TabItem = styled.TouchableOpacity`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const Label = styled.Text`
  font-size: 14px;
  color: ${(state) => (state.active ? '#EE9317' : '#8E8E94')};
`;
