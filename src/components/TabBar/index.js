import React from 'react';
import Icon from 'react-native-vector-icons/Entypo';

import {Container, TabItem, Label} from './styles';

const TabBar = ({navigation, state}) => {
  function goTo(screen) {
    navigation.navigate(screen);
  }

  return (
    <Container>
      <TabItem onPress={() => goTo('Counters')}>
        <Icon
          name="browser"
          size={22}
          color={state.index === 0 ? '#EE9317' : '#8E8E94'}
        />
        <Label active={state.index === 0 && true}>Counters</Label>
      </TabItem>
      <TabItem onPress={() => goTo('Controls')}>
        <Icon
          name="cog"
          size={25}
          color={state.index === 1 ? '#EE9317' : '#8E8E94'}
        />
        <Label active={state.index === 1 && true}>Config</Label>
      </TabItem>
    </Container>
  );
};

export default TabBar;
