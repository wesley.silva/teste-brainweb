import React from 'react';
import {Container, Name, Value} from './styles';
import {useDispatch, useSelector} from 'react-redux';

const Counter = ({data, index}) => {
  const dispatch = useDispatch();
  const {indexSelected} = useSelector((state) => state);

  const selectCounter = () => {
    dispatch({
      type: 'SELECT_COUNTER',
      payload: {
        index,
      },
    });
  };

  return (
    <Container active={indexSelected === index} onPress={selectCounter}>
      <Name active={indexSelected === index}>{'Counter ' + data.id}</Name>
      <Value active={indexSelected === index}>
        {data.value.toString().padStart(4, '0')}
      </Value>
    </Container>
  );
};

export default Counter;
