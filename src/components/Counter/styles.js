import styled from 'styled-components/native';

export const Container = styled.TouchableOpacity`
  width: 100%;
  padding: 20px;
  background: ${({active}) =>
    active ? 'rgba(255,255,255,1)' : 'rgba(110, 173, 208,1)'};
  height: 175px;
  justify-content: space-between;
  elevation: ${({active}) => (active ? 5 : 0)};
  border: ${({active}) =>
    active ? '4px solid rgba(0, 0, 0, 0.8)' : '4px solid rgba(0, 0, 0, 0.2)'};
  margin: 10px 0;
  border-radius: 5px;
`;

export const Name = styled.Text`
  font-size: 22px;
  font-weight: 700;
  color: ${({active}) => (active ? '#666' : '#888')};
`;

export const Value = styled.Text`
  font-size: 55px;
  font-weight: bold;
  color: ${({active}) => (active ? '#555' : '#888')};
  text-align: right;
`;
