import React from 'react';
import {useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/Entypo';

import {Button, TextButton} from './styles';

const OptionButton = ({title, onPress, name}) => {
  const {counters} = useSelector((state) => state);

  return (
    <Button
      disabled={counters.length === 0 && title !== 'Add counter' && true}
      onPress={onPress}>
      <Icon color="#254e82" size={22} name={name} />
      <TextButton>{title}</TextButton>
    </Button>
  );
};

export default OptionButton;
