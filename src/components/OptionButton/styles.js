import styled from 'styled-components/native';

export const Button = styled.TouchableOpacity`
  flex-direction: row;
  /* margin: 0 5px; */
  background: green;
  width: 167px;
  height: 70px;
  background: ${({disabled}) =>
    disabled ? 'rgba(216, 216, 216, 0.38)' : 'rgba(216, 216, 216,1)'};
  border-radius: 5px;
  justify-content: center;
  align-items: center;
  padding: 0 40px;
  box-shadow: 2px 2px 5px rgba(0, 28, 71, 0.3);
  elevation: 2;
`;

export const TextButton = styled.Text`
  color: #254e82;
  margin-left: 10px;
  text-align: center;
  font-size: 18px;
  font-weight: bold;
`;
