import React from 'react';
import {useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/Entypo';
import {ContainerButtonControl} from './styles';

const ControlButton = ({name, onPress}) => {
  const {counters, indexSelected} = useSelector((state) => state);

  const checkValue = () => {
    const {value} = counters[indexSelected];
    return value;
  };

  return (
    <ContainerButtonControl
      activeOpacity={0.5}
      onPress={onPress}
      type={name === 'ccw' && true}
      disabled={
        (counters.length === 0 && true) ||
        (name !== 'plus' && checkValue() === 0)
      }>
      <Icon name={name} size={30} color={name === 'ccw' ? '#fff' : '#001C47'} />
    </ContainerButtonControl>
  );
};

export default ControlButton;
