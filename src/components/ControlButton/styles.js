import styled from 'styled-components/native';

export const ContainerButtonControl = styled.TouchableOpacity`
  background: ${({type}) => (type ? '#001C47' : '#2983c9')};
  width: ${({type}) => (type ? '90px' : '60px')};
  border: ${({disabled}) =>
    disabled ? '2px solid #1c4585' : '2px solid #001c47'};
  height: ${({type}) => (type ? '90px' : '60px')};
  border-radius: 50px;
  justify-content: center;
  align-items: center;
  box-shadow: 2px 2px 5px rgba(0, 28, 71, 0.3);
  elevation: 5;
  opacity: ${({disabled}) => (disabled ? 0.5 : 1)};
`;

export const TextButtonControl = styled.Text`
  color: #204b80;
  text-align: center;
  font-weight: bold;
`;
