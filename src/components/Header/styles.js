import styled from 'styled-components/native';

export const Container = styled.View`
  height: ${({height}) => height + 'px'};
  /* min-height: 120px; */
  background: #001c47;
  padding: 20px;
  justify-content: flex-end;
`;

export const Text = styled.Text`
  font-size: 28px;
  font-weight: bold;
  color: #fff;
`;
