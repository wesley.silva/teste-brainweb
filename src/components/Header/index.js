import React from 'react';
import {Dimensions} from 'react-native';

import {Container, Text} from './styles.js';

export default function index({title}) {
  const {height} = Dimensions.get('window');

  return (
    <Container height={height * 0.15}>
      <Text>{title}</Text>
    </Container>
  );
}
