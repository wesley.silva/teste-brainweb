import React from 'react';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {PersistGate} from 'redux-persist/integration/react';

import CountersScreen from './src/screens/CountersScreen';
import ConfigScreen from './src/screens/ConfigScreen';

import TabBar from './src/components/TabBar';

import {store, persistor} from './src/store';

const Navigation = () => {
  const Tab = createBottomTabNavigator();

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <NavigationContainer>
          <Tab.Navigator tabBar={(props) => <TabBar {...props} />}>
            <Tab.Screen name="Counters" component={CountersScreen} />
            <Tab.Screen name="Controls" component={ConfigScreen} />
          </Tab.Navigator>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
};

export default Navigation;
