<h1 align="center">
    <img alt="Logo Brainweb" src="https://api.vulpi.com.br/media/logo_BW_PNG-02.png" width="250px" />
</h1>

<p align="center">
  <a href="#rocket-sobre">Sobre</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#collision-funcionalidades">Bibliotecas Utilizadas</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#rocket-tecnologias">Tecnologias</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#zap-rodando-o-projeto">Rodando o Projeto</a>
</p>

<h2>
App Contador.
</h2>

## 🚀 Sobre a aplicação

Aplicação Mobile Desenvolvida como teste para a vaga de Desenvolvedor Mobile da empresa Brainweb.

## :collision: Bibliotecas Utilizadas

- React Navigation | React Navigation Bottom Tabs: Bibliotecas responsáveis pela construção e navegação em diferentes telas na aplicação. 

- React Native Vector Icons: Biblioteca para a utilização de ícones.

- Redux, React-Redux, Redux-Persist e AsyncStorage: Bibliotecas utilizadas para compartilhamento global de estado e persistencia de dados. Foram utilizadas para armazenar os estados e os contadores de forma permanente, mesmo quando o usuário fechar a aplicação.

- Styled-components: Biblioteca utilizada para aplicar estilos aos componentes de React Native.

- React Native Tiny Toast: Biblioteca utilizada para feedbacks em formato de Toast.

## :rocket: Tecnologias

Esse projeto foi desenvolvido com a seguinte tecnologiaa:

- [React Native](https://reactnative.dev/)

\* Para mais detalhes, veja o <kbd>[package.json](./package.json)</kbd>

## :zap: Rodando o projeto

### Pré-requisitos

1 - Fazer o clone da aplicaçao;

2 - Instalar os pacotes com o comando yarn ou npm:

```bash
# Em um terminal, entrar na raiz do projeto e rodar o comando:
$ yarn
$ npm install

```

3 - Executar o comando pod install no diretório do ios:

```bash
$ cd ios && npx pod install

```

4 - Executar o projeto:
```bash
# Em um terminal, entrar na raiz do projeto e rodar o comando:
$ npx react-native run-ios          #Para dispositivo iOS  
$ npx react-native run-android      #Para dispositivo Android
```

---
